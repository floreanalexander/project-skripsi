import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
const Card = props => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={{
        marginVertical: 5,
        marginHorizontal: 20,
        backgroundColor: '#fefeff',
        elevation: 2,
        paddingVertical: 20,
        paddingHorizontal: 10,
        borderRadius: 6,
        flexDirection: 'row',
      }}
      onPress={() => navigation.navigate('DetailKata', {data: props.item})}>
      <Image
        style={{
          width: 50,
          height: 50,
          borderRadius: 6,
        }}
        source={{uri: props.item.image}}
      />
      <View style={{marginLeft: 10}}>
        <Text style={{fontWeight: 'bold'}}>{props.item.indonesia}</Text>
        <Text>{props.item.english}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
