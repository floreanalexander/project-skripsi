import React, {useState, useEffect} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

const BottomMenu = props => {
  const [activeMenu, setActiveMenu] = useState(props.screenName);
  const navigation = useNavigation();

  return (
    <View
      style={{
        paddingVertical: 10,
        backgroundColor: '#fefeff',
        borderTopWidth: 1,
        borderTopColor: '#bdbdbd',
        flexDirection: 'row',
      }}>
      <TouchableOpacity
        style={{justifyContent: 'center', alignItems: 'center', flex: 1}}
        onPress={() => navigation.navigate('Home')}>
        <Icon
          name="home"
          size={25}
          color={activeMenu == 'Home' ? '#6a8dff' : '#bdbdbd'}
        />
        <Text style={{fontSize: 12}}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{justifyContent: 'center', alignItems: 'center', flex: 1}}
        onPress={() => navigation.navigate('DaftarKata')}>
        <Icon
          name="book"
          size={25}
          color={activeMenu == 'DaftarKata' ? '#6a8dff' : '#bdbdbd'}
        />
        <Text style={{fontSize: 12}}>Daftar Kata</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{justifyContent: 'center', alignItems: 'center', flex: 1}}
        onPress={() => navigation.navigate('Profile')}>
        <Icon
          name="person"
          size={25}
          color={activeMenu == 'Profile' ? '#6a8dff' : '#bdbdbd'}
        />
        <Text style={{fontSize: 12}}>Profile</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BottomMenu;
