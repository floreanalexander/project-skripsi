import React, {useState, useEffect} from 'react';
import {View, Text, Image} from 'react-native';
import HeaderDetail from '../components/HeaderDetail';

const DetailKata = ({route}) => {
  const {data} = route.params;
  return (
    <View style={{flex: 1}}>
      <HeaderDetail title="Detail Kata" description={data.indonesia} />
      <View style={{flex: 1, marginTop: 10}}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{
              width: 350,
              height: 200,
              borderRadius: 6,
            }}
            source={{uri: data.image}}
          />
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <View
            style={{
              backgroundColor: '#303030',
              elevation: 2,
              borderRadius: 6,
              flex: 1,
              marginLeft: 20,
              marginRight: 5,
              padding: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#272727',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                width: 50,
                height: 50,
                marginRight: 10,
              }}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 6,
                }}
                source={require('../images/1.png')}
              />
            </View>
            <View>
              <Text style={{color: '#FFFFFF'}}>Indonesia</Text>
              <Text
                style={{fontSize: 18, fontWeight: 'bold', color: '#FFFFFF'}}>
                {data.indonesia}
              </Text>
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#303030',
              elevation: 2,
              borderRadius: 6,
              flex: 1,
              marginRight: 20,
              marginLeft: 5,
              padding: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#272727',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                width: 50,
                height: 50,
                marginRight: 10,
              }}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 6,
                }}
                source={require('../images/2.png')}
              />
            </View>
            <View>
              <Text style={{color: '#FFFFFF'}}>English</Text>
              <Text
                style={{fontSize: 18, fontWeight: 'bold', color: '#FFFFFF'}}>
                {data.english}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default DetailKata;
