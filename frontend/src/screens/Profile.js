import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import BottomMenu from '../components/BottomMenu';
import HeaderDetail from '../components/HeaderDetail';
import Icon from 'react-native-vector-icons/Ionicons';
const Profile = () => {
  const [menu, setMenu] = useState([
    {menu: 'Bahasa', deskripsi: 'ganti bahasa saat ini', icon: 'globe'},
    {menu: 'Dark Mode', deskripsi: 'ubah tema aplikasi', icon: 'moon-outline'},
    {
      menu: 'Tentang Kami',
      deskripsi: 'pengembang aplikasi ini',
      icon: 'code-slash',
    },
  ]);

  return (
    <View style={{flex: 1}}>
      <HeaderDetail title="Profile" description="profile anda" />
      <View style={{flex: 1}}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 20,
            backgroundColor: '#fefeff',
            paddingVertical: 20,
          }}>
          <Image
            style={{width: 80, height: 80, borderRadius: 40}}
            source={{
              uri: 'https://images.unsplash.com/photo-1514461713809-b245d3816ff1?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687',
            }}
          />
          <Text style={{fontWeight: 'bold', fontSize: 18, marginTop: 10}}>
            Phant Phin
          </Text>
          <Text>phantphin@gmail.com</Text>
          <TouchableOpacity
            style={{
              marginTop: 10,
              backgroundColor: '#6a8dff',
              paddingVertical: 15,
              paddingHorizontal: 30,
              borderRadius: 30,
            }}>
            <Text style={{color: '#FFFFFF'}}>Edit Profile</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={menu}
          style={{marginTop: 20}}
          renderItem={({item}) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: '#fefeff',
                paddingVertical: 10,
              }}>
              <Icon
                style={{marginHorizontal: 10}}
                name={item.icon}
                size={30}
                color={'#272727'}
              />
              <View style={{flex: 1}}>
                <Text style={{color: '#272727', fontWeight: 'bold'}}>
                  {item.menu}
                </Text>
                <Text style={{fontSize: 14}}>{item.deskripsi}</Text>
              </View>
              <Icon
                style={{marginHorizontal: 5}}
                name={'chevron-forward'}
                size={30}
                color={'#272727'}
              />
            </View>
          )}
        />
      </View>
      <BottomMenu screenName="Profile" />
    </View>
  );
};

export default Profile;
