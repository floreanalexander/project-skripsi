import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Header from '../components/Header';
import BottomMenu from '../components/BottomMenu';
import Card from '../components/Card';
// import data from '../data/test.json';

import axios from 'axios';

const Home = () => {
  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  // const [datas, setDataTampil] = useState(data);

  const [datas, setDataTampil] = useState([]);
  const getDatas = async () => {
    const response = await axios.get("http://localhost:5000/datas");
    setFilteredDataSource(response.data);
    setDataTampil(response.data);
  };

  useEffect(() => {
    getDatas();
  }, []);

  // useEffect(() => {
  //   setDataTampil(searchString(data, search));
  // }, [search]);

  //function searchString(array, string) {
  //  return array.filter(o =>
  //    Object.keys(o).some(k =>
  //      o[k].toLowerCase().includes(string.toLowerCase()),
  //    ),
  //  );
  //}
  
  const searchDataFunction = (text) => {
    if (text) {
      const newData = datas.filter(function (item){
        const itemData = item.indonesia
          ? item.indonesia.toLowerCase()
          : ''.toLowerCase();
        const textData = text.toLowerCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    }else{
      setFilteredDataSource(datas);
      setSearch(text);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fbfdff'}}>
      <Header title="Kamus Mini" description="Cari Terjemahan Kata ID ke ENG" />

      <TextInput
        value={search}
        onChangeText={(text) => searchDataFunction(text)}
        style={{
          backgroundColor: '#fefeff',
          elevation: 2,
          marginTop: 10,
          marginHorizontal: 20,
          borderRadius: 6,
          paddingLeft: 10,
        }}
        placeholder="Masukkan Kata yang Ingin Dicari"
      />
      <View style={{flex: 1}}>
        <FlatList
          data={filteredDataSource}
          style={{marginTop: 10}}
          renderItem={({item}) => <Card item={item} />}
        />
      </View>
      <BottomMenu screenName="Home" />
    </View>
  );
};

export default Home;
